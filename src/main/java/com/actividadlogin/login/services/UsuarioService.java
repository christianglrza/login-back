package com.actividadlogin.login.services;

import java.util.Optional;

import com.actividadlogin.login.models.Usuario;
import com.actividadlogin.login.repository.UsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {
    @Autowired
    public UsuarioRepository usuarioRepository;

    public Usuario guardarUsuario(Usuario usuario){
        return usuarioRepository.save(usuario);
    }
    
    public Optional<Usuario> findByUsername(String username){
        return usuarioRepository.findByUsername(username);
    }

    public Optional<Usuario> login(String username, String password){
        return usuarioRepository.login(username, password);
    }
}
