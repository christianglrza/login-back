package com.actividadlogin.login.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "role")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name_rol;

    public Role() {
    }

    public Role(Long id, String name_rol) {
        this.id = id;
        this.name_rol = name_rol;
    }

    public Role(Long id) {
        this.id = id; 
    }

    public Role(String name_rol) {
        this.name_rol = name_rol; 
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName_rol() {
        return name_rol;
    }

    public void setName_rol(String name_rol) {
        this.name_rol = name_rol;
    }  
}
