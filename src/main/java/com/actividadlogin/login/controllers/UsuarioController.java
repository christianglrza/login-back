package com.actividadlogin.login.controllers;

import java.util.Optional;

import com.actividadlogin.login.encryptConfig.JasyptEncryptorConfig;
import com.actividadlogin.login.models.Usuario;
import com.actividadlogin.login.services.UsuarioService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class UsuarioController {
    @Autowired
    UsuarioService usuarioService;

    @Autowired
    JasyptEncryptorConfig encrypter;

    @PostMapping("/usuario")
    public Usuario guardar(@RequestBody Usuario usuario){
        Usuario u = new Usuario();
        u.setUsername(usuario.getUsername());
        u.setPassword(encrypter.passwordEncryptor().encrypt(usuario.getPassword()).toString());
        return usuarioService.guardarUsuario(u);
    }

    @PostMapping("/get/usuario")
    public Optional<Usuario> findByUsername(@RequestBody Usuario usuario){
        return usuarioService.findByUsername(usuario.getUsername());
    }

    @PostMapping()
    public ResponseEntity<Optional<Usuario>> login(@RequestBody Usuario usuario){
        Optional<Usuario> usuarioEncontrado = usuarioService.findByUsername(usuario.getUsername());
        if(usuarioEncontrado.isPresent()){
            String password = usuarioEncontrado.map(Usuario::getPassword).orElse(null);
            String realPassword = encrypter.passwordEncryptor().decrypt(password).toString();
            String passwordEntry = usuario.getPassword();
            if(realPassword.equals(passwordEntry)){
                return ResponseEntity.ok(usuarioEncontrado);
            }else{
                return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
            }
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
