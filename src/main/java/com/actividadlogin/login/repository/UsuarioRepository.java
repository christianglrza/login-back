package com.actividadlogin.login.repository;

import java.util.Optional;

import com.actividadlogin.login.models.Usuario;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long>{
    @Query(value = "SELECT * FROM usuario WHERE username= ?1", nativeQuery = true)
    Optional<Usuario> findByUsername(String username);

    @Query(value = "SELECT * FROM usuario WHERE username= ?1 and password= ?2", nativeQuery = true)
    Optional<Usuario> login(String username, String password);

    
}