package com.actividadlogin.login;

import static org.assertj.core.api.Assertions.assertThat;

import com.actividadlogin.login.models.Role;
import com.actividadlogin.login.models.Usuario;
import com.actividadlogin.login.repository.RoleRepository;
import com.actividadlogin.login.repository.UsuarioRepository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.Rollback;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;
     
    @Autowired
    private UsuarioRepository userRepo;
     
    @Autowired
    private RoleRepository roleRepo;

    @Test
    public void testCreateUser() {
    Usuario user = new Usuario();
    user.setUsername("jugador");
    user.setPassword("1234");
     
    Usuario savedUser = userRepo.save(user);
     
    Usuario existUser = entityManager.find(Usuario.class, savedUser.getId());
     
    assertThat(user.getUsername()).isEqualTo(existUser.getUsername()); 
    } 

    @Test
    public void testAddRoleToExistingUser() {
        Usuario user = userRepo.findById(10L).get();
        Role roleEmpleado = new Role(2L);
        
        user.addRole(roleEmpleado);

        
        Usuario savedUser = userRepo.save(user);
        
        assertThat(savedUser.getRol().size()).isEqualTo(1);      
    } 
    
    
}